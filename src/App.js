import React, { Component } from 'react';
import './App.css';
import Tabs from "./Tabs";
import Tab from "./Tab";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
     value: null
    };
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({ value: e.target.elements.view.value })
  }
  render() {
    return (
      <div className="App">
        <h3>Tabs</h3>
        <Tabs>
        <Tab value={0} header="View 1">
          <form onSubmit={(e) => this.handleSubmit(e)}>
            <input type="text" defaultValue={this.state.value} name="view"/>
            <button type="submit">submit</button>
          </form>
        </Tab>
        <Tab value={1} header="View 2">
          <input defaultValue={this.state.value} />
        </Tab>
        <Tab value={2} header="View 3">
          view 3
        </Tab>
      </Tabs>
      </div>
    );
  }
}

export default App;
